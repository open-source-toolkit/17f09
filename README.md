# 中兴光猫权限开启与SSH设置修改指南

## 概述

本仓库提供了一个实用工具——`factorymode_crack.exe`，专为中兴光猫用户设计。通过此工具，您可以轻松获取中兴光猫的高级权限，进而启用SSH功能并调整如地区码等关键网络参数。这对于喜欢自定义网络配置或解决特定问题的进阶用户尤为有用。

## 使用步骤

1. **准备阶段**：确保您的电脑已连接到光猫所在的局域网内。默认情况下，光猫的IP地址为192.168.1.1。

2. **运行工具**：
   - 打开命令提示符（CMD）。
   - 转至存放`factorymode_crack.exe`的目录。
   - 输入以下命令尝试连接（假设默认设置，若端口或IP有变，请相应调整）:
     ```
     factorymode_crack.exe -l xxx open -i 192.168.1.1 -u telecomadmin -pw nE7jA%5m -p 8080
     ```
     注意：某些光猫可能使用8080作为管理端口，若-8080未生效，请直接尝试不带端口号或更改为实际使用的端口。
     
3. **权限验证**：
   - 成功执行上述命令后，如果看到类似于“Enter192.168.1.1FactoryModeSuccess...”的信息，表明已成功进入工厂模式，并会显示telnet登录所需的具体用户名和密码。
   - 用户名通常为`telnet`，默认密码会随程序输出提供，形如`user=telnetuser&pass=生成的密码`。

4. **利用telnet进行进一步操作**：
   - 使用获得的用户名和密码，通过telnet客户端连接到光猫（例如，使用命令`telnet 192.168.1.1`），现在您便可以自由地修改包括地区码在内的各种网络参数。

## 注意事项
- 在进行任何更改前，请确保您了解所作修改的含义，以防不当操作导致网络服务中断。
- `-l xxx`中的`xxx`是一个保留参数，目前无需具体值，但命令格式要求存在。
- 确保您的操作符合服务商的政策，避免因擅自修改而引起不必要的服务问题。
- 本工具仅用于合法的个人网络维护目的，请勿用于非法入侵他人设备。

## 结语
此工具的使用需谨慎，它为解锁光猫潜在功能提供了便利。正确应用这些步骤，您将能够更有效地管理您的家庭网络环境。祝您使用顺利！

--- 

请注意，技术文档随软件版本更新可能会有所变化，建议在使用前核实最新信息。